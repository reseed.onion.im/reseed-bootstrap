#!/usr/bin/env bash

set -e
export AWS_SHARED_CREDENTIALS_FILE=/var/local/reseed-keys/credentials
export AWS_DEFAULT_REGION=us-east-2
export AWS_DEFAULT_OUTPUT=json

TMP=$(mktemp -d)

tar -czf "$TMP"/netdb.tar.gz /var/local/netdb/
aws s3 cp "$TMP"/netdb.tar.gz s3://reseed-onion-im-us-east-2/

tar -czf "$TMP"/letsencrypt.tar.gz /etc/letsencrypt/
aws s3 cp "$TMP"/letsencrypt.tar.gz s3://reseed-onion-im-us-east-2/

rm -fr "$TMP"
