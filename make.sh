#!/usr/bin/env bash

set -e

if true
then
    echo "Uploading assets"
    CRED_RW=$(keepassxc-cli show -s ~/.keepass/chris-passwords.kdbx 'reseed-rw@barry.im')
    USER_RW=$(echo "$CRED_RW" | grep UserName | awk '{print $2}' -)
    PASS_RW=$(echo "$CRED_RW" | grep Password | awk '{print $2}' -)
    export AWS_ACCESS_KEY_ID="$USER_RW"
    export AWS_SECRET_ACCESS_KEY="$PASS_RW"
    export AWS_DEFAULT_REGION=us-east-2

    aws s3 cp --recursive ./config/ s3://reseed-onion-im-us-east-2/config/
fi

CRED_RO=$(keepassxc-cli show -s ~/.keepass/chris-passwords.kdbx 'reseed-ro@barry.im')
USER_RO=$(echo "$CRED_RO" | grep UserName | awk '{print $2}' -)
PASS_RO=$(echo "$CRED_RO" | grep Password | awk '{print $2}' -)

CRED_WO=$(keepassxc-cli show -s ~/.keepass/chris-passwords.kdbx 'reseed-wo@barry.im')
USER_WO=$(echo "$CRED_WO" | grep UserName | awk '{print $2}' -)
PASS_WO=$(echo "$CRED_WO" | grep Password | awk '{print $2}' -)

sed 's|RESEED_ACCESS_KEY|'"$USER_RO"'|g; s|RESEED_SECRET_KEY|'"$PASS_RO"'|g; s|RESEED_WO_USER|'"$USER_WO"'|g; s|RESEED_WO_PASS|'"$PASS_WO"'|g' ./bootstrap.sh.tmpl > bootstrap.sh

exit

DO_CRED=$(keepassxc-cli show -s ~/.keepass/chris-passwords.kdbx 'digital-ocean-api')
DO_PASS=$(echo "$DO_CRED" | grep Password | awk '{print $2}' -)

curl -X GET \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $DO_PASS" \
  "https://api.digitalocean.com/v2/account/keys"

exit
curl -X POST \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $DO_PASS" \
  --data-binary @- "https://api.digitalocean.com/v2/droplets" <<EOF
  {
       "name": "reseed-api",
       "region": "nyc3",
       "size": "s-1vcpu-1gb",
       "image": "debian-10-x64",
       "ssh_keys": [5169178],
       "backups": false,
       "ipv6": true,
       "user_data": null,
       "private_networking": true,
       "volumes": null,
       "tags":["i2p","all"]
  }
EOF
