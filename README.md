# reseed.onion.im

## Bootstrap

- bootstrap.sh - holds a script to make new reseed
- config/reseed.onion.im.conf - nginx config
- config/reseed.onion.im.html - simple static page for humans
- config/reseed-server.service - systemd service for i2p-tools

### Setting up

Expected to be run via user data.

## Policies

### DNS

- Two-factor authentication
- Hardware token
- Secure password
- Regular audits of Gandi access logs

### Digital Ocean

#### Backups

- `reseed.onion.im.crt` offsite backups checked monthly
- `reseed.onion.im.pem` offsite backups checked monthly
- `lazygravy_at_mail.i2p.pem` offsite backups checked monthly

#### Account Security

- Two-factor authentication
- Hardware token
- Secure password
- Regular audits of Digital Ocean access logs

#### Networking

- IPV6
- Firewall allow [::\0]:443
- Firewall allow 0.0.0.0:443
- Firewall allow [my-ip]:22

### Operating System

#### nginx

- No server tokens
- HTTP2
- TLS1.2+
- HTTPS only
- Custom Diffie Hellman params
- A static page for http clients on /index.html

#### Logging

- Log rotate clears out nginx logs every two days

#### Patching

- Debian 12
- Patches are all handled via Debian unattended upgrades

#### SSH

- `root` denied
- ed25519 key only
- SSH key log in only
- Different users for an admin and uploads

#### Services

- i2p-tools ran as separate user
- Process constrained by systemd
- Service only listened on localhost (i.e. all traffic proxies through nginx)
- Reseed files uploaded daily from offsite server via rsync+ssh

### Monitoring

Monitored via [reseed-lambda](https://gitlab.com/reseed.onion.im/reseed-bootstrap).

## License

[Creative Commons Zero v1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)
